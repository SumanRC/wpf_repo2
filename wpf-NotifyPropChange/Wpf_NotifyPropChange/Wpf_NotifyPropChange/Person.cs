﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf_NotifyPropChange
{
    public class Person : INotifyPropertyChanged
    {
        //Added from cloud server
        private string _firstname;
        private string _lastname;
        private string _fullname;
        private int _age;
        private string _address;
        private string _pin;
        private string _area;
        private string _city;
        private string _state;
        private string _pan;


        private string _passportno;

        public event PropertyChangedEventHandler PropertyChanged;

        public string FirstName
        {
            get { return _firstname; }

            set
            {
                _firstname = value;
                this.FullName = _firstname;

                if (!string.IsNullOrEmpty(_lastname))
                {
                    this.FullName = value + " " + _lastname;
                }

                //OnPropertyChanged("FirstName");
                //OnPropertyChanged("FullName");
            }
        }

        public string LastName
        {
            get { return _lastname; }

            set
            {
                _lastname = value;
                this.FullName += _lastname;

                if (!string.IsNullOrEmpty(_firstname))
                {
                    this.FullName = _firstname + " " + _lastname;
                }

                //OnPropertyChanged("LastName");
                //OnPropertyChanged("FullName");
            }
        }

        public string FullName
        {
            get { return _fullname; }

            set
            {
                _fullname = value;
                OnPropertyChanged("FullName");
            }
        }

        public string Address
        {
            get { return _address; }

            set
            {
                _address = value;
                OnPropertyChanged("Address");
            }
        }

        public string PinCode
        {
            get { return _pin; }

            set
            {
                _pin = value;
                OnPropertyChanged("PinCode");
            }
        }

        public string City
        {
            get { return _city; }

            set
            {
                _city = value;
                OnPropertyChanged("City");
            }
        }

        public string State
        {
            get { return _state; }

            set
            {
                _state = value;
                OnPropertyChanged("State");
            }
        }












        public string PassportNo
        {
            get { return _passportno; }

            set
            {
                _passportno = value;
                OnPropertyChanged("PassportNo");
            }
        }

        public string Pan
        {
            get { return _pan; }

            set
            {
                _pan = value;
                OnPropertyChanged("Pan");
            }
        }

        public Person()
        {
            //_firstname = "Indra";
            //_lastname = "Basu";
        }

        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
